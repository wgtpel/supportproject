﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace TronclassSupportProject
{
    /// <summary>
    /// download_docx 的摘要描述
    /// </summary>
    public class DownloadDocx : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string filePath = VirtualPathUtility.ToAbsolute("~/static/document/application_document.doc");
            DirectTransmitFile(context, filePath, "行動學習、智慧校園＿申請文件.doc");
        }

        /// <summary>
        /// 進行指定的檔案進行傳送
        /// </summary>
        /// <param name="context"></param>
        /// <param name="filePath">檔案路徑</param>
        /// <param name="fileName">檔案名稱</param>
        public void DirectTransmitFile(HttpContext context, string filePath, string fileName)
        {
            //context.Response.ContentType = "Application/application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            context.Response.ContentType = "Application/msword";
            EncodeHeaderName(context, fileName);
            context.Response.TransmitFile(filePath);  //下載檔案
            context.Response.End();
        }

        /// <summary>
        /// 為下載檔名編碼並加入 Header
        /// </summary>
        /// <param name="context"></param>
        /// <param name="headerName"></param>
        public void EncodeHeaderName(HttpContext context, string headerName)
        {
            if (ConfigurationManager.AppSettings["IEVersion"] != null)
            {
                string[] ieNames = ConfigurationManager.AppSettings["IEVersion"].Split(',');
                bool isIE = false;
                foreach (string ieName in ieNames)
                {
                    if (ieName.Contains(context.Request.Browser.Browser))
                    {
                        context.Response.AddHeader("Content-Disposition", String.Format("attachment; filename=\"{0}\"", HttpUtility.UrlEncode(headerName)));
                        isIE = true;
                    }
                }
                if (isIE == false)  // (Firefox、Chrome ...)
                    context.Response.AddHeader("Content-Disposition", String.Format("attachment; filename=\"{0}\"", headerName));
            }
            else
            {
                if (context.Request.Browser.Browser == "IE" || context.Request.Browser.Browser == "InternetExplorer")
                {
                    //context.Response.HeaderEncoding = System.Text.Encoding.GetEncoding("UTF-8");
                    context.Response.AddHeader("Content-Disposition", String.Format("attachment; filename=\"{0}\"", HttpUtility.UrlEncode(headerName)));
                }
                else    // (Firefox || Chrome)
                    context.Response.AddHeader("Content-Disposition", String.Format("attachment; filename=\"{0}\"", headerName));
            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}