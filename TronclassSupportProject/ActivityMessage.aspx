﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Base.Master" AutoEventWireup="true" CodeBehind="ActivityMessage.aspx.cs" Inherits="TronclassSupportProject.ActivityMessage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="static/js/FlexSliderController.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMenu" runat="server">
    <div class="row ">
        <div class="col-xs-6 ">
            &nbsp;
        </div>
        <div class="col-xs-2">
            <a href="~/ActivityMessage.aspx" runat="server" class="active">活動訊息</a>
        </div>
        <div class="col-xs-4 ">
            <a href="http://www.tronclass.com.tw/" style="text-align: right;" target="_blank">Tronclass 官網</a>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphImg" runat="server">
    <div class="flexslider">
        <ul class="slides">
            <li>
                <a runat="server" href="~/BigDataSeminar.aspx">
                    <img style="width: 100%;" src="~/static/images/data_banner_1899x420.png" alt="智慧教育大數據研討會" runat="server" />
                </a>
            </li>
            <li>
                <a runat="server" href="~/TronclassWorkshop.aspx">
                    <img style="width: 100%;" src="~/static/images/workshop_banner_1899x420b.png" alt="翻轉課堂工作坊" runat="server" />
                </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphBody" runat="server">
    <div class="content_message">
        <div class="row">
            <div class="col-xs-12 " style="margin-bottom: 20px;">
                <div style="font-size: 30px;" class="title">活動訊息</div>
            </div>
            <div>
                <div class="col-xs-4 text-center " style="padding: 0 10px 10px 10px;">
                    <div class="activity_content">
                        <div>
                            <a runat="server" href="~/BigDataSeminar.aspx">
                                <img src="~/static/images/data_banner_310X180_a.jpg" style="height: 150px;" runat="server" />
                            </a>
                        </div>
                        <div style="margin: 20px 0;">
                            智慧教育大數據研討會
                            <br />
                            2016 / 03 / 29
                            <br />
                            (
                            <a runat="server" href="~/BigDataSeminar.aspx">
                                查看活動紀錄
                            </a>
                            )
                        </div>
                    </div>
                </div>
                <div class="col-xs-4 text-center" style="padding: 0 10px 10px 10px;">
                    <div class="activity_content">
                        <div>
                            <a runat="server" href="~/TronclassWorkshopInTamsui.aspx">
                                <img src="~/static/images/workshop_banner_310X180_b.jpg" style="height: 150px;" runat="server" />
                            </a>
                        </div>
                        <div style="margin: 20px 0;">
                            TronClass 體驗工作坊：翻轉課堂
                            <br />
                            2016 / 04 / 18
                            <br />
                            ( 淡江大學校內場，
                            <a runat="server" href="~/TronclassWorkshopInTamsui.aspx">
                                查看活動紀錄
                            </a>
                            )
                        </div>
                    </div>
                </div>
                <div class="col-xs-4 text-center">
                    <div class="activity_content">
                        <div>
                            <a runat="server" href="~/TronclassWorkshop.aspx">
                                <img src="~/static/images/workshop_banner_310X180_b.jpg" style="height: 150px;" runat="server" />
                            </a>
                        </div>
                        <div style="margin: 20px 0;">
                            TronClass 體驗工作坊：翻轉課堂
                            <br />
                            2016 / 05 / 13
                            <br />
                            ( 淡江大學台北場，
                            <a runat="server" href="~/TronclassWorkshop.aspx">查看活動紀錄</a>
                            )
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
