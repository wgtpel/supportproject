﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Base.Master" AutoEventWireup="true" CodeBehind="BigDataSeminar.aspx.cs" Inherits="TronclassSupportProject.BigDataSeminar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".fancybox").fancybox({
                openEffect: 'none',
                closeEffect: 'none'
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMenu" runat="server">
    <div class="row ">
        <div class="col-xs-6 ">
            &nbsp;
        </div>
        <div class="col-xs-2">
            <a href="~/ActivityMessage.aspx" runat="server" class="active">活動訊息</a>
        </div>
        <div class="col-xs-4 ">
            <a href="http://www.tronclass.com.tw/" style="text-align: right;" target="_blank">Tronclass 官網</a>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphImg" runat="server">
    <a runat="server" href="~/BigDataSeminar.aspx">
        <img style="width: 100%;" src="static/images/data_banner_1899x420.png" alt="智慧教育大數據研討會" />
    </a>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphBody" runat="server">
    <div class="content_message">
        <div class="row">
            <div class="col-xs-12 " style="margin-bottom: 20px;">
                <div style="font-size: 30px; color: #4a5154; margin-bottom: 20px;" class="text-center">
                    智慧教育大數據研討會
                </div>
                <div>
                    近年來，資訊科技的快速發展，帶動了高等教育數位學習 (E-Learning) 的應用。
                    根據美國知名的高等教育研究機構 The Campus Computing Project 與 The Leadership Board for CIOs 2015 年的報告指出，
                    目前美國有近五成 (48%) 的大專院校使用行動服務；並有超過六成 (61%) 的學校已使用雲端數位學習平台。
                    此外根據歐洲最具權威的高等教育研究機構European University Association 2015 年的報告指出，
                    歐洲有高達七成 (74%) 的大專院校提供混合式學習課程。為促進臺灣大專院校在數位學習的發展上能與國際接軌，
                    中華民國數位學習學會與淡江大學資訊處、智園研究中心合作辦理「智慧教育大數據研討會」，
                    會中將發表 2015 年臺灣大專院校數位學習問卷普查研究結果，並將邀請相關領域專家擔任演講嘉賓，協助瞭解高等教育校園數位學習發展情形。
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <div class="col-xs-3">日期：</div>
                <div class="col-xs-9 subtitle">
                    <font size="5">2016 </font>年 <font size="5"> 3 </font>月 <font size="5"> 29 </font>日
                    <span style="padding-left: 10px;">星期二</span>
                </div>
                <div class="col-xs-3">時間：</div>
                <div class="col-xs-9">下午 1：30 至 5：00 </div>
                <div class="col-xs-3">地點：</div>
                <div class="col-xs-9">
                    台北市大安區金華街 199 巷 5 號 2 樓<br />
                    中正紀念堂會議廳。<br />
                    （淡江大學台北校區中正紀念堂會議廳）
                </div>
            </div>
            <div class="col-xs-6">
                <a target="_blank" href="https://www.google.com.tw/maps/place/%E6%B7%A1%E6%B1%9F%E5%A4%A7%E5%AD%B8%E5%8F%B0%E5%8C%97%E6%A0%A1%E5%8D%80/@25.0309089,121.5285146,18.75z/data=!4m3!3m2!1s0x3442a983a08598e1:0x4ce853dcac5e7ef0!4b1">
                    <img src="~/static/images/tku_taipei.jpg" style="width: 410px;" runat="server" />
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-2">聯繫方式：</div>
            <div class="col-xs-10">
                <img src="~/static/images/phone_36x36.png" runat="server" />&nbsp;&nbsp;&nbsp;&nbsp;(02)2620-9750  連小姐
                <br />
                <img src="~/static/images/mail_36x36.png" runat="server" />&nbsp;&nbsp;&nbsp;&nbsp;survey@ael.org.tw
                <br />
                <img src="~/static/images/fax_36x36.png" runat="server" />&nbsp;&nbsp;&nbsp;&nbsp;(02)2621-4550
            </div>
        </div>
        <div class="row">
            <div class="col-xs-2">活動議程：</div>
            <div class="col-xs-10">
                <div>
                    <table class="table table-bordered agenda_table" style="background: white;">
                        <tr>
                            <th style="width: 150px;">時間
                            </th>
                            <th style="width: 270px;">會議議程</th>
                            <th>主講人</th>
                        </tr>
                        <tr>
                            <td>
                                <div class="col-xs-12">13:00 ~ 13:30</div>
                            </td>
                            <td colspan="2">
                                <div class="col-xs-12">貴賓報到</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="col-xs-12">13:30 ~ 13:40</div>
                            </td>
                            <td>
                                <div class="col-xs-12">致歡迎詞</div>
                            </td>
                            <td>
                                <div class="col-xs-12">中華民國數位學習學會榮譽理事長</div>
                                <div class="col-xs-12">趙榮耀 博士</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="col-xs-12">13:40 ~ 14:20</div>
                            </td>
                            <td>
                                <div class="col-xs-12">數位學習大數據分析</div>
                            </td>
                            <td>
                                <div class="col-xs-12">銘傳大學</div>
                                <div class="col-xs-12">
                                    <asp:LinkButton ID="lkbDownload1" runat="server" OnClick="lkbDownload1_Click">
                                        王金龍 副校長
                                        <img src="static/images/ic_get_app_black_18dp_1x.png" style="padding-left: 10px;"  alt="王金龍_副校長.pdf" />
                                    </asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="col-xs-12">14:20 ~ 15:00</div>
                            </td>
                            <td>
                                <div class="col-xs-12">台灣高等教育數位學習大調查報告與學習大數據 – XAPI分析與應用分享</div>
                            </td>
                            <td>
                                <div class="col-xs-12">中華民國數位學習學會理事長</div>
                                <div class="col-xs-12">
                                    <asp:LinkButton ID="lkbDownload2" runat="server" OnClick="lkbDownload2_Click">
                                        林立傑 博士
                                    </asp:LinkButton>
                                    <label style="padding: 0 10px; margin: 0;">|</label>
                                    <asp:LinkButton runat="server" ID="lkbDownload6" OnClick="lkbDownload6_Click">
                                        演講簡報
                                    </asp:LinkButton>
                                    、
                                    <asp:LinkButton runat="server" ID="lkbDownload8" OnClick="lkbDownload8_Click">
                                        調查報告
                                    </asp:LinkButton>

                                    <img src="static/images/ic_get_app_black_18dp_1x.png" style="padding-left: 10px;" alt="林立傑_演講簡報.pdf" />
                                </div>
                                <div class="col-xs-12">台灣智園有限公司</div>
                                <div class="col-xs-12">
                                    <asp:LinkButton ID="lkbDownload5" runat="server" OnClick="lkbDownload5_Click">
                                        邢溢將 產品總監
                                    </asp:LinkButton>
                                    <label style="padding: 0 10px; margin: 0;">|</label>
                                    <asp:LinkButton runat="server" ID="lkbDownload7" OnClick="lkbDownload7_Click">
                                        演講簡報
                                    </asp:LinkButton>
                                    <img src="static/images/ic_get_app_black_18dp_1x.png" style="padding-left: 10px;" alt="邢溢將_演講簡報.pdf" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="col-xs-12">15:00 ~ 15:30</div>
                            </td>
                            <td colspan="2">
                                <div class="col-xs-12">休息時間</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="col-xs-12">15:30 ~ 16:00</div>
                            </td>
                            <td>
                                <div class="col-xs-12">行動學習與翻轉課堂的應用與案例分享</div>
                            </td>
                            <td>
                                <div class="col-xs-12">淡江大學教育科技學系</div>
                                <div class="col-xs-12">
                                    <asp:LinkButton ID="lkbDownload3" runat="server" OnClick="lkbDownload3_Click">
                                        張瓊穗 教授
                                        <img src="static/images/ic_get_app_black_18dp_1x.png" style="padding-left: 10px;"  alt="張瓊穗_教授.pdf" />
                                    </asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="col-xs-12">16:00 ~ 16:40</div>
                            </td>
                            <td>
                                <div class="col-xs-12">座談會：行動學習大未來</div>
                            </td>
                            <td>
                                <div class="col-xs-12">主持人：</div>
                                <div class="col-xs-12">中華民國數位學習學會理事長．林立傑 博士</div>
                                <%--<div class="col-xs-12">&nbsp;</div>--%>
                                <div class="col-xs-12">與談人：</div>
                                <div class="col-xs-12">銘傳大學．王金龍 副校長</div>
                                <div class="col-xs-12">
                                    <asp:LinkButton ID="lkbDownload4" runat="server" OnClick="lkbDownload4_Click">
                                        淡江大學．郭經華 資訊長
                                        <img src="static/images/ic_get_app_black_18dp_1x.png" style="padding-left: 10px;"  alt="郭經華_資訊長.pdf" />
                                    </asp:LinkButton>
                                </div>
                                <div class="col-xs-12">淡江大學．張瓊穗 教授</div>
                                <div class="col-xs-12">台灣智園．邢溢將 產品總監</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="col-xs-12">16:40 ~ 17:00</div>
                            </td>
                            <td colspan="2">
                                <div class="col-xs-12">交流與抽獎</div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="text-center">
                <%--<a href="http://enroll.tku.edu.tw/signup.aspx?cid=bigdata160329"
                    class="btn signup_button" style="font-size: 25px;">我要報名
                </a>--%>
                <label class="btn signup_button" style="font-size: 25px;">已截止</label>
            </div>
        </div>


        <div class="row">
            <div class="col-xs-2">活動照片：</div>
            <div class="col-xs-10" style="line-height: 100px;">
                <div>
                    <a class="fancybox" rel="group1" href="static/images/329/1-1.jpg">
                        <img class="active_photo" src="static/images/329/1-1.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/329/1-2.jpg">
                        <img class="active_photo" src="static/images/329/1-2.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/329/1-3.jpg">
                        <img class="active_photo" src="static/images/329/1-3.jpg" />
                    </a>
                </div>
                <div>
                    <a class="fancybox" rel="group1" href="static/images/329/2-1.jpg">
                        <img class="active_photo" src="static/images/329/2-1.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/329/2-2.jpg">
                        <img class="active_photo" src="static/images/329/2-2.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/329/2-3.jpg">
                        <img class="active_photo" src="static/images/329/2-3.jpg" />
                    </a>
                </div>
                <div>
                    <a class="fancybox" rel="group1" href="static/images/329/3-1.jpg">
                        <img class="active_photo" src="static/images/329/3-1.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/329/3-2.jpg">
                        <img class="active_photo" src="static/images/329/3-2.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/329/3-3.jpg">
                        <img class="active_photo" src="static/images/329/3-3.jpg" />
                    </a>
                </div>
                <div>
                    <a class="fancybox" rel="group1" href="static/images/329/4-1.jpg">
                        <img class="active_photo" src="static/images/329/4-1.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/329/4-2.jpg">
                        <img class="active_photo" src="static/images/329/4-2.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/329/4-3.jpg">
                        <img class="active_photo" src="static/images/329/4-3.jpg" />
                    </a>
                </div>
                <div>
                    <a class="fancybox" rel="group1" href="static/images/329/5-1.jpg">
                        <img class="active_photo" src="static/images/329/5-1.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/329/5-2.jpg">
                        <img class="active_photo" src="static/images/329/5-2.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/329/5-3.jpg">
                        <img class="active_photo" src="static/images/329/5-3.jpg" />
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-2">歷史活動：</div>
            <div class="col-xs-5">
                <a runat="server" href="~/TronclassWorkshop.aspx">
                    <img src="static/images/workshop_banner_310X180_b.jpg" runat="server" />
                </a>
            </div>
            <div class="col-xs-5">
                <div>
                    <label style="font-size: 18px;">TronClass 體驗工作坊：翻轉課堂</label>
                    <br />
                    日期：2016 年 05 月 13 日 星期五
                    <br />
                    時間：下午 1:30 至 4:30
                </div>
            </div>
        </div>

    </div>

</asp:Content>
