﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Base.Master" AutoEventWireup="true" CodeBehind="TronclassWorkshopInTamsui.aspx.cs" Inherits="TronclassSupportProject.TronclassWorkshopInTamsui" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".fancybox").fancybox({
                openEffect: 'none',
                closeEffect: 'none'
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMenu" runat="server">
    <div class="row ">
        <div class="col-xs-6 ">
            &nbsp;
        </div>
        <div class="col-xs-2">
            <a href="~/ActivityMessage.aspx" runat="server" class="active">活動訊息</a>
        </div>
        <div class="col-xs-4 ">
            <a href="http://www.tronclass.com.tw/" style="text-align: right;" target="_blank">Tronclass 官網</a>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphImg" runat="server">
    <a runat="server" href="~/TronclassWorkshop.aspx">
        <img style="width: 100%;" src="static/images/workshop_banner_1899x420b.png" alt="TronClass 體驗工作坊：翻轉課堂" />
    </a>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphBody" runat="server">
    <div class="content_message">
        <div class="row">
            <div class="col-xs-12 " style="margin-bottom: 20px;">
                <div style="font-size: 30px; color: #4a5154; margin-bottom: 20px;" class="text-center">
                    TronClass 體驗工作坊：翻轉課堂<br />
                    ( 淡江大學校內場 )
                </div>
            </div>
            <div class="col-xs-6">
                <div class="col-xs-3">日期：</div>
                <div class="col-xs-9 subtitle">
                    <font size="5">2016 </font>年 <font size="5"> 4 </font>月 <font size="5"> 18 </font>日
                    <span style="padding-left: 10px;">星期一</span>
                </div>
                <div class="col-xs-3">時間：</div>
                <div class="col-xs-9">下午 1：30 至 4：30 </div>
                <div class="col-xs-3">地點：</div>
                <div class="col-xs-9">
                    淡江大學淡水校區 驚聲紀念大樓 T307 教室
                </div>
            </div>
            <div class="col-xs-6"></div>
        </div>
        <div class="row">
                <div class="col-xs-2">活動議程：</div>
                <div class="col-xs-10">
                    <div>
                        <table class="table table-bordered agenda_table" style="background: white;">
                            <tr>
                                <th style="width: 200px;">時間
                                </th>
                                <th style="">活動流程</th>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-xs-12">13:00 ~ 13:30</div>
                                </td>
                                <td colspan="2">
                                    <div class="col-xs-12">貴賓報到</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-xs-12">13:30 ~ 14:00</div>
                                </td>
                                <td>
                                    <div class="col-xs-12">翻轉課堂新發現</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-xs-12">14:00 ~ 14:35</div>
                                </td>
                                <td>
                                    <div class="col-xs-12">活動一：創課大哉問</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-xs-12">14:35 ~ 14:50</div>
                                </td>
                                <td>
                                    <div class="col-xs-12">活動二：準備您的創課材料</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-xs-12">14:50 ~ 15:10</div>
                                </td>
                                <td colspan="2">
                                    <div class="col-xs-12">活動三：動手動腦錄製您的微課程</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-xs-12">15:10 ~ 15:30</div>
                                </td>
                                <td colspan="2">
                                    <div class="col-xs-12">休息時間</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-xs-12">15:30 ~ 16:10</div>
                                </td>
                                <td>
                                    <div class="col-xs-12">活動四：行動創課微體驗</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-xs-12">16:10 ~ 16:30</div>
                                </td>
                                <td>
                                    <div class="col-xs-12">交流與抽獎</div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        <div class="row">
            <div class="col-xs-2">活動照片：</div>
            <div class="col-xs-10" style="line-height: 100px;">
                <div>
                    <a class="fancybox" rel="group1" href="static/images/418/1-1.jpg">
                        <img class="active_photo" src="static/images/418/1-1.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/418/1-2.jpg">
                        <img class="active_photo" src="static/images/418/1-2.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/418/1-3.jpg">
                        <img class="active_photo" src="static/images/418/1-3.jpg" />
                    </a>
                </div>
                <div>
                    <a class="fancybox" rel="group1" href="static/images/418/2-1.jpg">
                        <img class="active_photo" src="static/images/418/2-1.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/418/2-2.jpg">
                        <img class="active_photo" src="static/images/418/2-2.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/418/2-3.jpg">
                        <img class="active_photo" src="static/images/418/2-3.jpg" />
                    </a>
                </div>
                <div>
                    <a class="fancybox" rel="group1" href="static/images/418/3-1.jpg">
                        <img class="active_photo" src="static/images/418/3-1.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/418/3-2.jpg">
                        <img class="active_photo" src="static/images/418/3-2.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/418/3-3.jpg">
                        <img class="active_photo" src="static/images/418/3-3.jpg" />
                    </a>
                </div>
                <div>
                    <a class="fancybox" rel="group1" href="static/images/418/4-1.jpg">
                        <img class="active_photo" src="static/images/418/4-1.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/418/4-2.jpg">
                        <img class="active_photo" src="static/images/418/4-2.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/418/4-3.jpg">
                        <img class="active_photo" src="static/images/418/4-3.jpg" />
                    </a>
                </div>
                <div>
                    <a class="fancybox" rel="group1" href="static/images/418/5-1.jpg">
                        <img class="active_photo" src="static/images/418/5-1.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/418/5-2.jpg">
                        <img class="active_photo" src="static/images/418/5-2.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/418/5-3.jpg">
                        <img class="active_photo" src="static/images/418/5-3.jpg" />
                    </a>
                </div>
                <div>
                    <a class="fancybox" rel="group1" href="static/images/418/6-1.jpg">
                        <img class="active_photo" src="static/images/418/6-1.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/418/6-2.jpg">
                        <img class="active_photo" src="static/images/418/6-2.jpg" />
                    </a>
                </div>
            </div>
        </div>
        <%--<div class="row">
            <div class="col-xs-2">預約訊息：</div>
            <div class="col-xs-5">
                <label style="font-size: 18px;">TronClass 體驗工作坊：翻轉課堂</label>
                <br />
                同校教師人數若達30人，歡迎團體報名！
            </div>
            <div class="col-xs-5">
                <div>
                    <img src="static/images/phone_36x36.png" />
                    連嘉婷 (02)2620-9750
                    <br />
                    <img src="static/images/fax_36x36.png" />
                    (02)2621-4550 
                    <br />
                    <img src="static/images/mail_36x36.png" />
                    survey@ael.org.tw
                </div>
            </div>
        </div>--%>
        <div class="row">
            <div class="col-xs-2">歷史活動：</div>
            <div class="col-xs-5">
                <a runat="server" href="~/TronclassWorkshop.aspx">
                    <img src="static/images/workshop_banner_310X180_b.jpg" runat="server" />
                </a>
            </div>
            <div class="col-xs-5">
                <div>
                    <label style="font-size: 18px;">TronClass 體驗工作坊：翻轉課堂</label>
                    <br />
                    日期：2016 年 05 月 13 日 星期五
                    <br />
                    時間：下午 1:30 至 4:30
                    <br />
                    (
                    <a runat="server" href="~/TronclassWorkshop.aspx">淡江大學台北校區 D504 教室</a>
                    )
                </div>
            </div>
        </div>

    </div>
</asp:Content>
