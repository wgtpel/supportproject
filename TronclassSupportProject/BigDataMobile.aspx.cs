﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TronclassSupportProject
{
    public partial class BigDataMobile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 進行指定的檔案進行傳送
        /// </summary>
        /// <param name="context"></param>
        /// <param name="filePath">檔案路徑</param>
        /// <param name="fileName">檔案名稱</param>
        public void DirectTransmitFile(string filePath, string fileName)
        {
            Response.ContentType = "Application/pdf";
            EncodeHeaderName(fileName);
            Response.TransmitFile(filePath);  //下載檔案
            Response.End();
        }

        /// <summary>
        /// 為下載檔名編碼並加入 Header
        /// </summary>
        /// <param name="context"></param>
        /// <param name="headerName"></param>
        public void EncodeHeaderName(string headerName)
        {
            if (ConfigurationManager.AppSettings["IEVersion"] != null)
            {
                string[] ieNames = ConfigurationManager.AppSettings["IEVersion"].Split(',');
                bool isIE = false;
                foreach (string ieName in ieNames)
                {
                    if (ieName.Contains(Request.Browser.Browser))
                    {
                        Response.AddHeader("Content-Disposition", String.Format("attachment; filename=\"{0}\"", HttpUtility.UrlEncode(headerName)));
                        isIE = true;
                    }
                }
                if (isIE == false)  // (Firefox、Chrome ...)
                    Response.AddHeader("Content-Disposition", String.Format("attachment; filename=\"{0}\"", headerName));
            }
            else
            {
                if (Request.Browser.Browser == "IE" || Request.Browser.Browser == "InternetExplorer")
                {
                    //Response.HeaderEncoding = System.Text.Encoding.GetEncoding("UTF-8");
                    Response.AddHeader("Content-Disposition", String.Format("attachment; filename=\"{0}\"", HttpUtility.UrlEncode(headerName)));
                }
                else    // (Firefox || Chrome)
                    Response.AddHeader("Content-Disposition", String.Format("attachment; filename=\"{0}\"", headerName));
            }
        }

        protected void lkbDownload1_Click(object sender, EventArgs e)
        {
            string filePath = VirtualPathUtility.ToAbsolute("~/static/document/Seminar_Instructor_Introduction_1.pdf");
            DirectTransmitFile(filePath, "王金龍.pdf");
        }

        protected void lkbDownload2_Click(object sender, EventArgs e)
        {
            string filePath = VirtualPathUtility.ToAbsolute("~/static/document/Seminar_Instructor_Introduction_2.pdf");
            DirectTransmitFile(filePath, "林立傑.pdf");
        }

        protected void lkbDownload3_Click(object sender, EventArgs e)
        {
            string filePath = VirtualPathUtility.ToAbsolute("~/static/document/Seminar_Instructor_Introduction_3.pdf");
            DirectTransmitFile(filePath, "張瓊穗.pdf");

        }

        protected void lkbDownload4_Click(object sender, EventArgs e)
        {
            string filePath = VirtualPathUtility.ToAbsolute("~/static/document/Seminar_Instructor_Introduction_4.pdf");
            DirectTransmitFile(filePath, "郭經華.pdf");

        }


    }
}