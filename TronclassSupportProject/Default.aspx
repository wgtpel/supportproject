﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Base.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TronclassSupportProject.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-59224628-8', 'auto');
        ga('send', 'pageview');

    </script>
</asp:Content>
<asp:Content ID="Contnet3" ContentPlaceHolderID="cphMenu" runat="server">
    <div class="row ">
        <div class="col-xs-2">
            <a href="<%: VirtualPathUtility.ToAbsolute("~/Default.aspx") %>" class="active">首頁</a>
        </div>
        <div class="col-xs-2 header_menu">
            <a href="#project">計劃介紹</a>
        </div>
        <div class="col-xs-2">
            <a href="#support">補助辦法</a>
        </div>
        <div class="col-xs-2">
            <a href="~/ActivityMessage.aspx" runat="server">活動訊息</a>
        </div>
        <div class="col-xs-2">
            <a href="http://www.tronclass.com.tw/" target="_blank">平台介紹</a>
        </div>
        <div class="col-xs-2">
            <a href="~/ApplySupport.aspx" runat="server">申請補助</a>
        </div>
    </div>
</asp:Content>
<asp:Content ID="C4" ContentPlaceHolderID="cphImg" runat="server">
    <div class="flexslider">
        <ul class="slides">
            <li>
                <a runat="server" href="~/BigDataSeminar.aspx">
                    <img style="width: 100%;" src="~/static/images/data_banner_1899x420.png" alt="智慧教育大數據研討會" runat="server" />
                </a>
            </li>
            <li>
                <a runat="server" href="~/TronclassWorkshop.aspx">
                    <img style="width: 100%;" src="~/static/images/workshop_banner_1899x420b.png" alt="翻轉課堂工作坊" runat="server" />
                </a>
            </li>
        </ul>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
    <div class="content_message">
        <div class="row">
            <div class="title" style="font-size: 30px;">最新消息</div>
            <div class="col-xs-12">
                <div class="col-xs-6">
                    <div class="title">
                        <div class="col-xs-2 text-center">
                            <span style="font-size: 40px;">29</span>
                        </div>
                        <div class="col-xs-10" style="font-size: 18px;">
                            <a href="~/BigDataSeminar.aspx" runat="server">智慧教育大數據研討會</a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="title">
                        <div class="col-xs-2 text-center">
                            <span style="font-size: 40px;">13</span>
                        </div>
                        <div class="col-xs-10" style="font-size: 18px;">
                            <a href="~/TronclassWorkshop.aspx" runat="server">TronClass 體驗工作坊：翻轉課堂</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-6">
                    <div>
                        <div class="col-xs-2 title text-center">
                            <span>3 月</span>
                        </div>
                        <div class="col-xs-10">
                            105 . 03 . 29 中華民國數位學習學會於淡大台北校區舉辦 「智慧教育大數據研討會」，
                            歡迎全國各大專院校之正/副校長、資訊中心、教務處等數位學習相關負責人蒞臨。
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div>
                        <div class="col-xs-2 title text-center">
                            <span>5 月</span>
                        </div>
                        <div class="col-xs-10">
                            105 . 05 . 13 中華民國數位學習學會於淡大台北校區舉辦 「TronClass 體驗工作坊：翻轉課堂」，歡迎全國各大專院校之院長、系主任、教師共同參與。
                        </div>
                    </div>
                </div>
            </div>
            <a name="project"></a>
        </div>
        <div class="row">
            <div class="title" style="font-size: 30px;">計劃簡介</div>
            <div class="col-xs-2">
                <img src="~/static/images/smile_96x96.png" alt="" runat="server" />
            </div>
            <div class="col-xs-10">
                根據美國知名的高等教育研究機構 The Campus Computing Project 與 The Leadership Board for CIOs 2015 年的報告指出
                ，目前美國有近五成（48%）的大專院校使用行動服務；並有超過六成（61%）的學校已使用雲端數位學習平台。
                反觀臺灣，根據中華民國大專校院資訊服務協會 2015 年發表的「臺灣大專院校資訊單位組織及經費合理性調查研究報告」指出，
                國內雖有六成以上的大學已使用行動服務，但多數仍未將其應用於教學上（學習資源 : 38%、教學管理 : 28%）；且僅有一成多（15%）的學校使用雲端數位學習平台。
                <br />
                <br />
                因此，為確保臺灣大專院校在數位學習的發展上能與國際接軌，中華民國數位學習學會希望透過「行動學習、智慧校園」獎勵補助計畫，
                提供大專院校申請 TronClass 行動數位學習平台，通過中華民國數位學習學會審核並正式使用者，由台灣智園有限公司（以下簡稱智園）提供獎勵補助。
                <br />
                <span class="glyphicon glyphicon-triangle-right" aria-hidden="true" style="padding-right: 20px;"></span>
                系統平台承租費用減免 50%。【如下補助辦法】
                <br />
                <span class="glyphicon glyphicon-triangle-right" aria-hidden="true" style="padding-right: 20px;"></span>
                各校使用者於指定期間內依行動化學習平台使用活躍度及開課數量前五名者<br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;，本學會頒以獎狀以茲表揚。（指定時間為 105 / 5 / 1 ~ 105 / 10 / 31）
                <br />
                <span class="glyphicon glyphicon-triangle-right" aria-hidden="true" style="padding-right: 20px;"></span>
                協助使用者進行研究或發表行動化學習平台之應用成果。
                
                <a name="support"></a>
            </div>
        </div>
        <div class="row">
            <div class="title" style="font-size: 30px;">補助辦法</div>
            <div class="col-xs-2">
                <img src="~/static/images/card_96x96.png" alt="" runat="server" />
            </div>
            <div class="col-xs-10">
                <div class="subtitle">
                    <div class="col-xs-1">一、</div>
                    <div class="col-xs-11">申請方式：</div>
                </div>
                <div>
                    <div class="col-xs-1"></div>
                    <div class="col-xs-11">
                        由大專院校向本學會提出申請文件
                        <a href="~/DownloadDocx.ashx" runat="server" style="text-decoration: none;">【下載申請文件】</a>
                        ，並經審核通過後，轉介資訊予智園提供服務。
                    </div>
                </div>
                <div class="subtitle">
                    <div class="col-xs-1">二、</div>
                    <div class="col-xs-11">服務提供：</div>
                </div>
                <div>
                    <div class="col-xs-1"></div>
                    <div class="col-xs-11">
                        由智園（含經銷商）與大專院校承接，配合學校流程執行，並提供大專院校行動化學習平台建置服務。
                        行動化學習平台服務內容如下：
                        <br />
                        <span class="glyphicon glyphicon-triangle-right" aria-hidden="true" style="padding-right: 20px;"></span>
                        LMS 數位學習管理平台 ( 教師教學模組、學生學習模組、基礎教務管理模組
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;、統計分析模組 )
                        <br />
                        <span class="glyphicon glyphicon-triangle-right" aria-hidden="true" style="padding-right: 20px;"></span>
                        APP 行動學習模組 ( iOS & Android 版本 )
                        <br />
                        <span class="glyphicon glyphicon-triangle-right" aria-hidden="true" style="padding-right: 20px;"></span>
                        MOOCs 磨課師模組
                        <br />
                        <span class="glyphicon glyphicon-triangle-right" aria-hidden="true" style="padding-right: 20px;"></span>
                        平台免費升級 ( 於計畫期間內，每學年至少升級一次 )
                        <br />
                        <span class="glyphicon glyphicon-triangle-right" aria-hidden="true" style="padding-right: 20px;"></span>
                        提供 LMS 系統架構規劃建議 ( 硬體架構圖、軟硬體規格 )
                        <br />
                        <span class="glyphicon glyphicon-triangle-right" aria-hidden="true" style="padding-right: 20px;"></span>
                        提供 6 小時教育訓練，依對象不同分兩梯次實施，每次 3 小時

                    </div>
                </div>
                <div class="subtitle">
                    <div class="col-xs-1">三、</div>
                    <div class="col-xs-11">使用期限：</div>
                </div>
                <div>
                    <div class="col-xs-1"></div>
                    <div class="col-xs-11">自行動化學習平台建置並驗收完成之日起算一年為限。本計畫對象於使用期限內得以本計畫續租行動化學習平台。</div>
                </div>
                <div class="subtitle">
                    <div class="col-xs-1">四、</div>
                    <div class="col-xs-11">服務方案租用選擇及年度費用說明如下表：</div>
                </div>
                <div>
                    <div class="col-xs-1"></div>
                    <div class="col-xs-11">
                        <table class="table" style="line-height: 40px;">
                            <tr>
                                <th colspan="3">服務方案</th>
                                <th style="width: 200px;">公有雲（SaaS）</th>
                                <th>私有雲</th>
                            </tr>
                            <tr>
                                <td colspan="3">伺服器位置</td>
                                <td>智園雲端機房（台灣）</td>
                                <td>學校機房</td>
                            </tr>
                            <tr>
                                <td colspan="3">版本升級方式</td>
                                <td>自動即時更新</td>
                                <td>寒暑假或學期結束期間，經校方同意後更新</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 100px;">方案類型</td>
                                <td style="width: 160px;">學校人數</td>
                                <td>價　　格</td>
                                <td>價　　格</td>
                            </tr>
                            <tr>
                                <td rowspan="3" class="shuli">費用說明</td>
                                <td>Ａ方案</td>
                                <td>約 8000 人以下</td>
                                <td>
                                    <div class="delete_word_line">(原　　價) 70萬元</div>
                                    <div class="preferential_word">(優惠價格) 35萬元</div>
                                </td>
                                <td>
                                    <div class="delete_word_line">(原　　價) 60萬元</div>
                                    <div class="preferential_word">(優惠價格) 30萬元</div>
                                </td>
                            </tr>
                            <tr>
                                <td>Ｂ方案</td>
                                <td>約 8000 ~ 15000 人</td>
                                <td>
                                    <div class="delete_word_line">(原　　價) 90萬元</div>
                                    <div class="preferential_word">(優惠價格) 45萬元</div>
                                </td>
                                <td>
                                    <div class="delete_word_line">(原　　價) 80萬元</div>
                                    <div class="preferential_word">(優惠價格) 40萬元</div>
                                </td>
                            </tr>
                            <tr>
                                <td>Ｃ方案</td>
                                <td>約 15000 人以上</td>
                                <td>
                                    <div class="delete_word_line">(原　　價) 110萬元</div>
                                    <div class="preferential_word">(優惠價格) 55萬元</div>
                                </td>
                                <td>
                                    <div class="delete_word_line">(原　　價) 100萬元</div>
                                    <div class="preferential_word">(優惠價格) 50萬元</div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
</asp:Content>
