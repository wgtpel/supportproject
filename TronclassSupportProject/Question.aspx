﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Base.Master" AutoEventWireup="true" CodeBehind="Question.aspx.cs" Inherits="TronclassSupportProject.Question" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMenu" runat="server">
    <div class="row ">
        <div class="col-xs-2">
            <a href="<%: VirtualPathUtility.ToAbsolute("~/Default.aspx") %>">首頁</a>
        </div>
        <div class="col-xs-2 header_menu">
            <a href="<%: VirtualPathUtility.ToAbsolute("~/Default.aspx/#project") %>">計劃介紹</a>
        </div>
        <div class="col-xs-2">
            <a href="<%: VirtualPathUtility.ToAbsolute("~/Default.aspx/#support") %>">補助辦法</a>
        </div>
        <div class="col-xs-2">
            <a href="~/ActivityMessage.aspx" runat="server">活動訊息</a>
        </div>
        <div class="col-xs-2">
            <a href="http://www.tronclass.com.tw/" target="_blank">平台介紹</a>
        </div>
        <div class="col-xs-2">
            <a href="~/ApplySupport.aspx" runat="server">申請補助</a>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphImg" runat="server">
    <div class="flexslider">
        <ul class="slides">
            <li>
                <a runat="server" href="~/BigDataSeminar.aspx">
                    <img style="width: 100%;" src="~/static/images/data_banner_1899x420.png" alt="智慧教育大數據研討會" runat="server" />
                </a>
            </li>
            <li>
                <a runat="server" href="~/TronclassWorkshop.aspx">
                    <img style="width: 100%;" src="~/static/images/workshop_banner_1899x420b.png" alt="翻轉課堂工作坊" runat="server" />
                </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphBody" runat="server">
    <div class="content_message">

        <div class="question">
            <div class="row">
                <div class="col-xs-12 " style="margin: 20px 0;">
                    <div style="font-size: 30px; color: #4a5154;">常見問題</div>
                </div>
            </div>
            <div class="row subtitle">
                <div class="col-xs-1">
                    1、
                </div>
                <div class="col-xs-11 subtitle_left ">
                    申請資格？
                </div>
            </div>
            <div class="row">
                <div class="col-xs-1"></div>
                <div class="col-xs-11 question_content">
                    <div class="col-xs-12">凡設立於台灣本島及離島之大專院校及開設於大專院校之院系均為適用對象。</div>
                </div>
            </div>
            <div class="row subtitle">
                <div class="col-xs-1">
                    2、
                </div>
                <div class="col-xs-11 subtitle_left">
                    如果已經是 TronClass 用戶，是否還可以申請此計畫？
                </div>
            </div>
            <div class="row">
                <div class="col-xs-1"></div>
                <div class="col-xs-11 question_content">
                    <div class="col-xs-12">可以。</div>
                </div>
            </div>
            <div class="row subtitle">
                <div class="col-xs-1">
                    3、
                </div>
                <div class="col-xs-11 subtitle_left">
                    申請時間？
                </div>
            </div>
            <div class="row">
                <div class="col-xs-1"></div>
                <div class="col-xs-11 question_content">
                    <div class="col-xs-12">105 年 3 月 4 日至 105 年 8 月 31 日止。</div>
                </div>
            </div>
            <div class="row subtitle">
                <div class="col-xs-1">
                    4、
                </div>
                <div class="col-xs-11 subtitle_left">
                    如果提交申請表後還希望修改資料怎麼辦？
                </div>
            </div>
            <div class="row">
                <div class="col-xs-1"></div>
                <div class="col-xs-11 question_content">
                    <div class="col-xs-12">
                        請於繳交文件後 1 日內，以 E-mail 方式重新補交申請表。
                    </div>
                    <div class="col-xs-2">連 絡 人：</div>
                    <div class="col-xs-10">連嘉婷</div>
                    <div class="col-xs-2">電　　話：</div>
                    <div class="col-xs-10">（02）2620-9750</div>
                    <div class="col-xs-2">信　　箱：</div>
                    <div class="col-xs-10">survey@ael.org.tw</div>
                </div>
            </div>
            <div class="row subtitle">
                <div class="col-xs-1">
                    5、
                </div>
                <div class="col-xs-11 subtitle_left">
                    如果在現有的獎勵補助計畫外，還需要額外的服務怎麼辦？
                </div>
            </div>
            <div class="row">
                <div class="col-xs-1"></div>
                <div class="col-xs-11 question_content">
                    <div class="col-xs-12">當確認貴單位通過獎勵補助計畫後，台灣智園有限公司會安排專業人員到校諮詢並確認需求。</div>
                </div>
            </div>
            <div class="row subtitle">
                <div class="col-xs-1">
                    6、
                </div>
                <div class="col-xs-11 subtitle_left">
                    明年是否還可以續用本補助計畫？
                </div>
            </div>
            <div class="row" style="margin-bottom: 40px;">
                <div class="col-xs-1"></div>
                <div class="col-xs-11 question_content">
                    <div class="col-xs-12">
                        自行動化學習平台建置並驗收完成之日起算一年為限。
                        本計畫對象於使用期限內得以本計畫續租行動化學習平台。
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
