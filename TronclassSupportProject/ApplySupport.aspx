﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Base.Master" AutoEventWireup="true" CodeBehind="ApplySupport.aspx.cs" Inherits="TronclassSupportProject.ApplySupport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#imgFileDownload').show();
            $('#<%= linkFileDownload.ClientID %>').hide();

            $('#imgFileDownload').mouseover(function () {
                $(this).hide();
                $('#<%= linkFileDownload.ClientID %>').show();
            });
            $('#<%= linkFileDownload.ClientID %>').mouseout(function () {
                $(this).hide();
                $('#imgFileDownload').show();
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMenu" runat="server">
    <div class="row ">
        <div class="col-xs-2">
            <a href="<%: VirtualPathUtility.ToAbsolute("~/Default.aspx") %>">首頁</a>
        </div>
        <div class="col-xs-2 header_menu">
            <a href="<%: VirtualPathUtility.ToAbsolute("~/Default.aspx/#project") %>">計劃介紹</a>
        </div>
        <div class="col-xs-2">
            <a href="<%: VirtualPathUtility.ToAbsolute("~/Default.aspx/#support") %>">補助辦法</a>
        </div>
        <div class="col-xs-2">
            <a href="~/ActivityMessage.aspx" runat="server">活動訊息</a>
        </div>
        <div class="col-xs-2">
            <a href="http://www.tronclass.com.tw/" target="_blank">平台介紹</a>
        </div>
        <div class="col-xs-2">
            <a href="~/ApplySupport.aspx" runat="server" class="active">申請補助</a>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphImg" runat="server">
    <div class="flexslider">
        <ul class="slides">
            <li>
                <a runat="server" href="~/BigDataSeminar.aspx">
                    <img style="width: 100%;" src="~/static/images/data_banner_1899x420.png" alt="智慧教育大數據研討會" runat="server" />
                </a>
            </li>
            <li>
                <a runat="server" href="~/TronclassWorkshop.aspx">
                    <img style="width: 100%;" src="~/static/images/workshop_banner_1899x420b.png" alt="翻轉課堂工作坊" runat="server" />
                </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphBody" runat="server">
    <div class="content_message">
        <div class="row">
            <div class="col-xs-12 " style="margin-bottom: 20px;">
                <div style="font-size: 30px; margin-bottom: 20px;">申請補助流程</div>

                <table class="table_borderless">
                    <tr>
                        <td>
                            <img id="imgFileDownload" src="static/images/apply_1a_365x270.png" style="height: 214px;" />
                            <a id="linkFileDownload" style="display: none;" href="~/DownloadDocx.ashx" runat="server">
                                <img src="static/images/apply_1b_365x270.png" style="height: 214px;" />
                            </a>
                        </td>
                        <td>
                            <img src="static/images/apply_2_807x270.png" style="height: 214px;" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="static/images/apply_3_365x265.png" style="height: 210px;" />
                        </td>
                        <td>
                            <img src="static/images/apply_4_807x265.png" style="height: 210px;" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
