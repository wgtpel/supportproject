﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BigDataMobile.aspx.cs" Inherits="TronclassSupportProject.BigDataMobile" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=1;" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="static/images/favicon.ico.png" />
    <title>TronClass 獎勵補助計畫</title>

    <link href="<%: VirtualPathUtility.ToAbsolute("~/Content/bootstrap.min.css") %>" rel="stylesheet" />
    <link href="static/css/main.css" rel="stylesheet" />
    <script src="<%: VirtualPathUtility.ToAbsolute("~/Scripts/jquery-1.9.1.min.js") %>"></script>
    <script src="<%: VirtualPathUtility.ToAbsolute("~/Scripts/bootstrap.min.js") %>"></script>

    <style type="text/css">
        .bigdata_icon {
            width: 620px;
            height: 360px;
            background: url("static/images/data_banner_310X180_a.jpg");
            background-size: contain;
            background-repeat: no-repeat;
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" style="width: 620px;">
        <div class="bigdata_icon">
        </div>
        <div class="" style="width: 620px;">
            <table class="table table-bordered agenda_table" style="background: white;">
                <tr>
                    <th style="width: 140px;">時間
                    </th>
                    <th style="width: 170px;">會議議程</th>
                    <th>主講人</th>
                </tr>
                <tr>
                    <td>
                        <div class="col-xs-12">13:00 ~ 13:30</div>
                    </td>
                    <td colspan="2">
                        <div class="col-xs-12">貴賓報到</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="col-xs-12">13:30 ~ 13:40</div>
                    </td>
                    <td>
                        <div class="col-xs-12">致歡迎詞</div>
                    </td>
                    <td>
                        <div class="col-xs-12">中華民國數位學習學會榮譽理事長</div>
                        <div class="col-xs-12">趙榮耀 博士</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="col-xs-12">13:40 ~ 14:20</div>
                    </td>
                    <td>
                        <div class="col-xs-12">數位學習大數據分析</div>
                    </td>
                    <td>
                        <div class="col-xs-12">銘傳大學</div>
                        <div class="col-xs-12">
                            <asp:LinkButton ID="lkbDownload1" runat="server" OnClick="lkbDownload1_Click">
                                王金龍 副校長
                                <img src="static/images/ic_get_app_black_18dp_1x.png" style="padding-left: 10px;"  alt="王金龍_副校長.pdf" />
                            </asp:LinkButton>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="col-xs-12">14:20 ~ 15:00</div>
                    </td>
                    <td>
                        <div class="col-xs-12">台灣高等教育數位學習大調查報告與學習大數據 – XAPI分析與應用分享</div>
                    </td>
                    <td>
                        <div class="col-xs-12">中華民國數位學習學會理事長</div>
                        <div class="col-xs-12">
                            <asp:LinkButton ID="lkbDownload2" runat="server" OnClick="lkbDownload2_Click">
                                林立傑 博士
                                <img src="static/images/ic_get_app_black_18dp_1x.png" style="padding-left: 10px;"  alt="林立傑_博士.pdf" />
                            </asp:LinkButton>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="col-xs-12">15:00 ~ 15:20</div>
                    </td>
                    <td colspan="2">
                        <div class="col-xs-12">休息時間</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="col-xs-12">15:20 ~ 16:00</div>
                    </td>
                    <td>
                        <div class="col-xs-12">行動學習與翻轉課堂的應用與案例分享</div>
                    </td>
                    <td>
                        <div class="col-xs-12">淡江大學教育科技學系</div>
                        <div class="col-xs-12">
                            <asp:LinkButton ID="lkbDownload3" runat="server" OnClick="lkbDownload3_Click">
                                張瓊穗 教授
                                <img src="static/images/ic_get_app_black_18dp_1x.png" style="padding-left: 10px;"  alt="張瓊穗_教授.pdf" />
                            </asp:LinkButton>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="col-xs-12">16:00 ~ 16:40</div>
                    </td>
                    <td>
                        <div class="col-xs-12">座談會：行動學習大未來</div>
                    </td>
                    <td>
                        <div class="col-xs-12">主持人：</div>
                        <div class="col-xs-12">中華民國數位學習學會理事長</div>
                        <div class="col-xs-12">．林立傑 博士</div>
                        <%--<div class="col-xs-12">&nbsp;</div>--%>
                        <div class="col-xs-12">與談人：</div>
                        <div class="col-xs-12">銘傳大學．王金龍 副校長</div>
                        <div class="col-xs-12">
                            <asp:LinkButton ID="lkbDownload4" runat="server" OnClick="lkbDownload4_Click">
                                淡江大學．郭經華 資訊長
                                <img src="static/images/ic_get_app_black_18dp_1x.png" style="padding-left: 10px;"  alt="郭經華_資訊長.pdf" />
                            </asp:LinkButton>
                        </div>
                        <div class="col-xs-12">淡江大學教育科技學系．張瓊穗 教授</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="col-xs-12">16:40 ~ 17:00</div>
                    </td>
                    <td colspan="2">
                        <div class="col-xs-12">交流與抽獎</div>
                    </td>
                </tr>
            </table>
        </div>

        <div class="row" style="line-height: 30px;">
            <div class="title" style="font-size: 30px; padding-left: 15px; padding-bottom: 15px;">「行動數位學習校園」獎勵補助辦法：</div>
            <div class="col-xs-12">
                <div class="subtitle">
                    <div class="col-xs-1">一、</div>
                    <div class="col-xs-11">申請方式：</div>
                </div>
                <div>
                    <div class="col-xs-1"></div>
                    <div class="col-xs-11">
                        由大專院校向本學會提出申請文件
                        ，並經審核通過後，轉介資訊予智園提供服務。
                    </div>
                </div>
                <div class="subtitle">
                    <div class="col-xs-1">二、</div>
                    <div class="col-xs-11">服務提供：</div>
                </div>
                <div>
                    <div class="col-xs-1"></div>
                    <div class="col-xs-11">
                        由智園（含經銷商）與大專院校承接，配合學校流程執行，並提供大專院校行動化學習平台建置服務。
                        行動化學習平台服務內容如下：
                        <br />
                        <span class="glyphicon glyphicon-triangle-right" aria-hidden="true" style="padding-right: 20px;"></span>
                        LMS 數位學習管理平台 ( 教師教學模組、學生學習模組、基礎教務管理
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;模組、統計分析模組 )
                        <br />
                        <span class="glyphicon glyphicon-triangle-right" aria-hidden="true" style="padding-right: 20px;"></span>
                        APP 行動學習模組 ( iOS & Android 版本 )
                        <br />
                        <span class="glyphicon glyphicon-triangle-right" aria-hidden="true" style="padding-right: 20px;"></span>
                        MOOCs 磨課師模組
                        <br />
                        <span class="glyphicon glyphicon-triangle-right" aria-hidden="true" style="padding-right: 20px;"></span>
                        平台免費升級 ( 於計畫期間內，每學年至少升級一次 )
                        <br />
                        <span class="glyphicon glyphicon-triangle-right" aria-hidden="true" style="padding-right: 20px;"></span>
                        提供 LMS 系統架構規劃建議 ( 硬體架構圖、軟硬體規格 )
                        <br />
                        <span class="glyphicon glyphicon-triangle-right" aria-hidden="true" style="padding-right: 20px;"></span>
                        提供 6 小時教育訓練，依對象不同分兩梯次實施，每次 3 小時

                    </div>
                </div>
                <div class="subtitle">
                    <div class="col-xs-1">三、</div>
                    <div class="col-xs-11">使用期限：</div>
                </div>
                <div>
                    <div class="col-xs-1"></div>
                    <div class="col-xs-11">自行動化學習平台建置並驗收完成之日起算一年為限。本計畫對象於使用期限內得以本計畫續租行動化學習平台。</div>
                </div>
                <div class="subtitle">
                    <div class="col-xs-1">四、</div>
                    <div class="col-xs-11">服務方案租用選擇及年度費用說明如下表：</div>
                </div>
                <div>
                    <div class="col-xs-1"></div>
                    <div class="col-xs-11">
                        <table class="table" style="line-height: 40px;">
                            <tr>
                                <th colspan="3">服務方案</th>
                                <th style="width: 160px;">公有雲（SaaS）</th>
                                <th>私有雲</th>
                            </tr>
                            <tr>
                                <td colspan="3">伺服器位置</td>
                                <td>智園雲端機房<br />（台灣）</td>
                                <td>學校機房</td>
                            </tr>
                            <tr>
                                <td colspan="3">版本升級方式</td>
                                <td>自動即時更新</td>
                                <td>寒暑假或學期結束期間，經校方同意後更新</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 80px;">方案類型</td>
                                <td style="width: 100px;">學校人數</td>
                                <td>價　　格</td>
                                <td>價　　格</td>
                            </tr>
                            <tr>
                                <td rowspan="3" class="shuli">費用說明</td>
                                <td>Ａ方案</td>
                                <td>約 8000 人以下</td>
                                <td>
                                    <div class="delete_word_line">(原　　價) 70萬元</div>
                                    <div class="preferential_word">(優惠價格) 35萬元</div>
                                </td>
                                <td>
                                    <div class="delete_word_line">(原　　價) 60萬元</div>
                                    <div class="preferential_word">(優惠價格) 30萬元</div>
                                </td>
                            </tr>
                            <tr>
                                <td>Ｂ方案</td>
                                <td>約 8000 ~ 15000 人</td>
                                <td>
                                    <div class="delete_word_line">(原　　價) 90萬元</div>
                                    <div class="preferential_word">(優惠價格) 45萬元</div>
                                </td>
                                <td>
                                    <div class="delete_word_line">(原　　價) 80萬元</div>
                                    <div class="preferential_word">(優惠價格) 40萬元</div>
                                </td>
                            </tr>
                            <tr>
                                <td>Ｃ方案</td>
                                <td>約 15000 人以上</td>
                                <td>
                                    <div class="delete_word_line">(原　　價) 110萬元</div>
                                    <div class="preferential_word">(優惠價格) 55萬元</div>
                                </td>
                                <td>
                                    <div class="delete_word_line">(原　　價) 100萬元</div>
                                    <div class="preferential_word">(優惠價格) 50萬元</div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                
                <div class="text-center" style="padding-bottom: 30px;">
                    <a runat="server" href="~/Default.aspx" target="_blank" style="font-size: 20px;" class="btn signup_button">更多內容</a>
                </div>

            </div>
        </div>

    </form>
</body>
</html>
