﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Base.Master" AutoEventWireup="true" CodeBehind="TronclassWorkshop.aspx.cs" Inherits="TronclassSupportProject.TronclassWorkshop" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".fancybox").fancybox({
                openEffect: 'none',
                closeEffect: 'none'
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMenu" runat="server">
    <div class="row ">
        <div class="col-xs-6 ">
            &nbsp;
        </div>
        <div class="col-xs-2">
            <a href="~/ActivityMessage.aspx" runat="server" class="active">活動訊息</a>
        </div>
        <div class="col-xs-4 ">
            <a href="http://www.tronclass.com.tw/" style="text-align: right;" target="_blank">Tronclass 官網</a>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphImg" runat="server">
    <a runat="server" href="~/TronclassWorkshop.aspx">
        <img style="width: 100%;" src="static/images/workshop_banner_1899x420b.png" alt="TronClass 體驗工作坊：翻轉課堂" />
    </a>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphBody" runat="server">
    <div class="content_message">
        <div class="row">
            <div class="col-xs-12 " style="margin-bottom: 20px;">
                <div style="font-size: 30px; color: #4a5154; margin-bottom: 20px;" class="text-center">
                    TronClass 體驗工作坊：翻轉課堂
                </div>
                <div>
                    根據國際知名的教育研究機構 The New Media Consortium 2015 年發表的《Horizon Reports 高等教育版》報告中預測，
                    在近一年內（或者更短的時間），BYOD（Bring Your Own Device）與翻轉課堂 (Flipped Classroom) 將被高等教育機構採用。
                    此外，根據歐洲最具權威的高等教育研究機構 European University Association 2015 年的報告亦指出，
                    歐洲有高達七成 (74%) 的大專院校提供混合式學習課程。因此為促進臺灣大專院校在數位學習發展上能與國際接軌，
                    中華民國數位學習學會與淡江大學資訊處、智園研究中心合作辦理「TronClass 體驗工作坊：翻轉課堂」，並邀請行業領域專家擔任講師，帶領學員一同體驗翻轉課堂的魅力。
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <div class="col-xs-3">日期：</div>
                <div class="col-xs-9 subtitle">
                    <font size="5">2016 </font>年 <font size="5"> 5 </font>月 <font size="5"> 13 </font>日
                    <span style="padding-left: 10px;">星期五</span>
                </div>
                <div class="col-xs-3">時間：</div>
                <div class="col-xs-9">下午 1：30 至 4：30 </div>
                <div class="col-xs-3">地點：</div>
                <div class="col-xs-9">
                    台北市大安區金華街 199 巷 5 號 5 樓<br />
                    D504 教室。<br />
                    （淡江大學台北校區 D504 教室）
                </div>
            </div>
            <div class="col-xs-6">
                <a target="_blank" href="https://www.google.com.tw/maps/place/%E6%B7%A1%E6%B1%9F%E5%A4%A7%E5%AD%B8%E5%8F%B0%E5%8C%97%E6%A0%A1%E5%8D%80/@25.0309089,121.5285146,18.75z/data=!4m3!3m2!1s0x3442a983a08598e1:0x4ce853dcac5e7ef0!4b1">
                    <img id="Img1" src="~/static/images/tku_taipei.jpg" style="width: 410px;" runat="server" />
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-2">聯繫方式：</div>
            <div class="col-xs-10">
                <img id="Img2" src="~/static/images/phone_36x36.png" runat="server" />&nbsp;&nbsp;&nbsp;&nbsp;(02)2620-9750  連小姐
                <br />
                <img id="Img3" src="~/static/images/mail_36x36.png" runat="server" />&nbsp;&nbsp;&nbsp;&nbsp;survey@ael.org.tw
                <br />
                <img id="Img4" src="~/static/images/fax_36x36.png" runat="server" />&nbsp;&nbsp;&nbsp;&nbsp;(02)2621-4550
            </div>
        </div>
        <div class="row">
            <div class="row">
                <div class="col-xs-2">講師：</div>

                <div class="col-xs-10">
                    <asp:LinkButton ID="lkbDownload1" runat="server" OnClick="lkbDownload1_Click">
                        淡江大學教育科技學系副教授兼系主任 沈俊毅 博士
                        <img src="static/images/ic_get_app_black_18dp_1x.png" style="padding-left: 10px;"  alt="沈俊毅_博士.pdf" />
                    </asp:LinkButton>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-2">活動議程：</div>
                <div class="col-xs-10">
                    <div>
                        <table class="table table-bordered agenda_table" style="background: white;">
                            <tr>
                                <th style="width: 200px;">時間
                                </th>
                                <th style="">活動流程</th>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-xs-12">13:00 ~ 13:30</div>
                                </td>
                                <td colspan="2">
                                    <div class="col-xs-12">貴賓報到</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-xs-12">13:30 ~ 14:00</div>
                                </td>
                                <td>
                                    <div class="col-xs-12">翻轉課堂新發現</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-xs-12">14:00 ~ 14:35</div>
                                </td>
                                <td>
                                    <div class="col-xs-12">活動一：創課大哉問</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-xs-12">14:35 ~ 14:50</div>
                                </td>
                                <td>
                                    <div class="col-xs-12">活動二：準備您的創課材料</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-xs-12">14:50 ~ 15:10</div>
                                </td>
                                <td colspan="2">
                                    <div class="col-xs-12">活動三：動手動腦錄製您的微課程</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-xs-12">15:10 ~ 15:30</div>
                                </td>
                                <td colspan="2">
                                    <div class="col-xs-12">休息時間</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-xs-12">15:30 ~ 16:10</div>
                                </td>
                                <td>
                                    <div class="col-xs-12">活動四：行動創課微體驗</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-xs-12">16:10 ~ 16:30</div>
                                </td>
                                <td>
                                    <div class="col-xs-12">交流與抽獎</div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="text-center">
                <%--<a href="http://enroll.tku.edu.tw/signup.aspx?cid=workshop160421&l=c"
                    class="btn signup_button not_yet_start" style="font-size: 25px;">我要報名
                </a>--%>
                <label class="btn signup_button" style="font-size: 25px;">已截止</label>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-2">活動照片：</div>
            <div class="col-xs-10" style="line-height: 100px;">
                <div>
                    <a class="fancybox" rel="group1" href="static/images/513/1-1.jpg">
                        <img class="active_photo" src="static/images/513/1-1.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/513/1-2.jpg">
                        <img class="active_photo" src="static/images/513/1-2.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/513/1-3.jpg">
                        <img class="active_photo" src="static/images/513/1-3.jpg" />
                    </a>
                </div>
                <div>
                    <a class="fancybox" rel="group1" href="static/images/513/2-1.jpg">
                        <img class="active_photo" src="static/images/513/2-1.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/513/2-2.jpg">
                        <img class="active_photo" src="static/images/513/2-2.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/513/2-3.jpg">
                        <img class="active_photo" src="static/images/513/2-3.jpg" />
                    </a>
                </div>
                <div>
                    <a class="fancybox" rel="group1" href="static/images/513/3-1.jpg">
                        <img class="active_photo" src="static/images/513/3-1.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/513/3-2.jpg">
                        <img class="active_photo" src="static/images/513/3-2.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/513/3-3.jpg">
                        <img class="active_photo" src="static/images/513/3-3.jpg" />
                    </a>
                </div>
                <div>
                    <a class="fancybox" rel="group1" href="static/images/513/4-1.jpg">
                        <img class="active_photo" src="static/images/513/4-1.jpg" />
                    </a>
                    <a class="fancybox" rel="group1" href="static/images/513/4-2.jpg">
                        <img class="active_photo" src="static/images/513/4-2.jpg" />
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-2">歷史活動：</div>
            <div class="col-xs-5">
                <a runat="server" href="~/TronclassWorkshopInTamsui.aspx">
                    <img src="static/images/workshop_banner_310X180_b.jpg" runat="server" />
                </a>
            </div>
            <div class="col-xs-5">
                <div>
                    <label style="font-size: 18px;">TronClass 體驗工作坊：翻轉課堂</label>
                    <br />
                    日期：2016 年 04 月 18 日 星期一
                    <br />
                    時間：下午 1:30 至 4:30
                    <br />
                    (
                    <a runat="server" href="~/TronclassWorkshopInTamsui.aspx">淡江大學淡水校區 驚聲紀念大樓</a>
                    )
                </div>
            </div>
        </div>
    </div>

</asp:Content>
